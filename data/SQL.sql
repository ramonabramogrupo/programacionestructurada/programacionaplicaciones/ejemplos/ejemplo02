﻿DROP DATABASE IF EXISTS ejemplo2aplicacion;
CREATE DATABASE ejemplo2aplicacion;
USE ejemplo2aplicacion;


CREATE TABLE productos(
id int AUTO_INCREMENT,
nombre varchar(200),
peso float,
PRIMARY KEY(id)
);

CREATE TABLE clientes(
id int AUTO_INCREMENT,
nombre varchar(200),
PRIMARY KEY(id)
);

CREATE TABLE compran(
idCompran int AUTO_INCREMENT,
producto int,
cliente int,
fecha date,
cantidad int,
PRIMARY KEY(idCompran),
UNIQUE KEY(producto,cliente,fecha)

);

ALTER TABLE compran 
  ADD CONSTRAINT fkcompranclientes FOREIGN KEY (cliente) REFERENCES clientes(id)
    on DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT fkcompranproductos FOREIGN KEY (producto) REFERENCES productos(id)
  on DELETE CASCADE ON UPDATE CASCADE