<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compran".
 *
 * @property int $idCompran
 * @property int|null $producto
 * @property int|null $cliente
 * @property string|null $fecha
 * @property int|null $cantidad
 *
 * @property Clientes $cliente0
 * @property Productos $producto0
 */
class Compran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto', 'cliente', 'cantidad'], 'integer'],
            [['fecha'], 'safe'],
            [['producto', 'cliente', 'fecha'], 'unique', 'targetAttribute' => ['producto', 'cliente', 'fecha']],
            [['cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::class, 'targetAttribute' => ['cliente' => 'id']],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['producto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCompran' => 'Id Compran',
            'producto' => 'Producto',
            'cliente' => 'Cliente',
            'fecha' => 'Fecha',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[Cliente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente0()
    {
        return $this->hasOne(Clientes::class, ['id' => 'cliente']);
    }

    /**
     * Gets query for [[Producto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(Productos::class, ['id' => 'producto']);
    }
}
